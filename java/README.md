# Cicero-java
Java layer of the Cicero - ConsIstent seCurE pRactical cOntroller

## Setup

Set up RSA key pairs for controllers so that ssh works without a password

Install java components
 * sudo apt-get install ant
 * sudo apt-get install default-jdk

Install Ryu 4.23 for python

Edit config/hosts.config to set the IP addresses of all replicas

Edit config/cicero.config with correct configuration parameters for replicas and location of certificates used for controller to controller communication needed for DKG
