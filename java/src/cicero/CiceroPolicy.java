/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.util.Iterator;
import java.util.LinkedList;


class CiceroRuleDependence
{
	private int xDpIndex;
	private long xXid;

	CiceroRuleDependence(int dp, long xid) 
	{
		xDpIndex = dp;
		xXid = xid;
	}

	public final int getDpIndex()
	{
		return xDpIndex;
	}

	public final long getXID()
	{
		return xXid;
	}
}

/**
 * Cicero update rule structure
 *
 */

class CiceroRule
{
	private int xDpIndex;
	private OpenFlowMessage xMsg;
	private CiceroRuleDependence xFlowDep;
	private CiceroRuleDependence xSwitchDep;

	CiceroRule(int dp, OpenFlowMessage msg, CiceroRuleDependence fld, CiceroRuleDependence swd)
	{
		xDpIndex = dp;
		xMsg = msg;
		xFlowDep = fld;
		xSwitchDep = swd;
	}

	public final int getDpIndex()
	{
		return xDpIndex;
	}

	public OpenFlowMessage getMessage()
	{
		return xMsg;
	}

	public final CiceroRuleDependence getFlowDep()
	{
		return xFlowDep;
	}

	public final CiceroRuleDependence getSwitchDep()
	{
		return xSwitchDep;
	}
}

public class CiceroPolicy
{
	private LinkedList<CiceroRule> xRules;

	public CiceroPolicy()
	{
		xRules = new LinkedList<CiceroRule>();
	}

	public boolean isEmpty()
	{
		return xRules.isEmpty();
	}

	public void pushRule(CiceroRule rule)
	{
		synchronized(xRules) {
			xRules.add(rule);
		}
	}

	public CiceroRule pullRule()
	{
		synchronized(xRules) {
			if(xRules.isEmpty()) {
				return null;
			}
			try {
				return xRules.remove();
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public CiceroRule getNextRule()
	{
		synchronized(xRules) {
			if(xRules.isEmpty()) {
				return null;
			}
			try {
				return xRules.getFirst();
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
