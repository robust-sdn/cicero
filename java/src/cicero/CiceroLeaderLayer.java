/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.io.IOException;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import java.util.Map;
import java.util.HashMap;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetSocketAddress;

import it.unisa.dia.gas.jpbc.Element;
/**
 * Cicero Leader Handler
 *
 */

class CiceroLeaderConnection implements Runnable
{
	private Socket xLeaderSocket;
	private InetSocketAddress xLeaderAddress;
	private OpenFlowMessageWriter xToLeader;

	public CiceroLeaderConnection(String host, int port)
	{
		try {
			xLeaderSocket = new Socket();
			xLeaderAddress = new InetSocketAddress(host, port);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public boolean isConnected()
	
	{
		return xLeaderSocket.isConnected();
	}

	public void connect()
	{
		if(xLeaderSocket.isConnected()) {
			disconnect();
		}
		new Thread(this).start();
	}

	public void run()
	{
		while(true) {
			try {
				CiceroLogger.logInfo("Connecting to leader: " + xLeaderAddress);
				xLeaderSocket.connect(xLeaderAddress);
				xToLeader = new OpenFlowMessageWriter(xLeaderSocket.getOutputStream());
				break;
			} catch(Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(1000);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void disconnect()
	{
		try {
			CiceroLogger.logInfo("Disconnecting from leader: " + xLeaderAddress);
			xLeaderSocket.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void sendLeader(OpenFlowMessage ofMsg)
	{
		if(xToLeader.write(ofMsg) == false) {
			disconnect();
		}
	}
}

class CiceroOpenFlowBLSSignatureSet
{
	private OpenFlowMessage xSource;
	private int xDpIndex;
	private Set<CiceroBLSSignature> xSignatures;

	public CiceroOpenFlowBLSSignatureSet(OpenFlowMessage source, int dp_idx)
	{
		xSource = source;
		xDpIndex = dp_idx;
		xSignatures = new HashSet<CiceroBLSSignature>();
	}

	public OpenFlowMessage getMessage()
	{
		return xSource;
	}

	public int getDpIndex()
	{
		return xDpIndex;
	}

	public int addSignature(CiceroBLSSignature sig)
	{
		xSignatures.add(sig);
		return xSignatures.size();
	}

	public Set<CiceroBLSSignature> getSignatures()
	{
		return xSignatures;
	}
}

class CiceroSignatureMap
{
	private List< Map<Integer, CiceroOpenFlowBLSSignatureSet> > xSignatureListMap;

	public CiceroSignatureMap()
	{
		xSignatureListMap = new ArrayList< Map<Integer, CiceroOpenFlowBLSSignatureSet> >();
	}

	public synchronized void addDataPath(int dp_idx)
	{
		xSignatureListMap.add(dp_idx, new HashMap<Integer, CiceroOpenFlowBLSSignatureSet>());
	}

	public void remove(OpenFlowBLSLeaderMessage msg)
	{
		Map<Integer, CiceroOpenFlowBLSSignatureSet> sig_map = xSignatureListMap.get(msg.getDpIndex());
		sig_map.remove(msg.getXID());	
	}

	public CiceroOpenFlowBLSSignatureSet get(OpenFlowBLSLeaderMessage msg)
	{
		Map<Integer, CiceroOpenFlowBLSSignatureSet> sig_map = xSignatureListMap.get(msg.getDpIndex());
		synchronized(sig_map) {
			CiceroOpenFlowBLSSignatureSet sig_set = sig_map.get(msg.getXID());
			if(sig_set == null) {
				sig_set = new CiceroOpenFlowBLSSignatureSet(msg.getMessage(), msg.getDpIndex());
				sig_map.put(msg.getXID(), sig_set);
			}
			return sig_set;
		}
	}
}

public class CiceroLeaderLayer
{
	private NewPeerConnectionThread xPeerConnectionThread;
	private List<PeerMessageThread> xPeerConnections;
	private List<ProxyConnection> xSwitchConnections;
	private CiceroSignatureMap xSignatureMap;
	private CiceroBLSAggregator xBLSAggregator;
	private CiceroBLSVerifier xBLSVerifier;
	private int xQuorumSize;
	private Set<Integer> xRetiredXids;

	public CiceroLeaderLayer(CiceroConfigProperties props, List<ProxyConnection> conns)
	{
		xPeerConnections = new ArrayList<PeerMessageThread>();
		xPeerConnectionThread = new NewPeerConnectionThread(props.getLeaderPort());
		xSwitchConnections = conns;
		xSignatureMap = new CiceroSignatureMap();
		xBLSAggregator = new CiceroBLSAggregator(props);
		xBLSVerifier = new CiceroBLSVerifier(props);
		xRetiredXids = new HashSet<Integer>();
		xQuorumSize = 4;
	}

	public void start()
	{
		CiceroLogger.logInfo("Starting Cicero Leader Layer...");
		xPeerConnectionThread.start();
	}

	public void stop()
	{
		xPeerConnectionThread.stop();
		for (PeerMessageThread peer : xPeerConnections) {
			peer.stop();
		}
	}

	public void stopAndWait()
	{
		xPeerConnectionThread.stopAndWait();
		for (PeerMessageThread peer : xPeerConnections) {
			peer.stopAndWait();
		}
	}

	public void waitDone()
	{
		xPeerConnectionThread.waitDone();
		for (PeerMessageThread peer : xPeerConnections) {
			peer.waitDone();
		}
	}

	public void addDataPath(int dp_idx)
	{
		xSignatureMap.addDataPath(dp_idx);	
	}

	class NewPeerConnectionThread implements Runnable
	{
		private int xPort;
		private boolean xRunning;
		private ServerSocket xServerSocket;
		private Thread xThread;

		public NewPeerConnectionThread(int port)
		{
			xPort = port;
			xRunning = false;
		}

		public void start()
		{
			xRunning = true;
			xThread = new Thread(this, "CICERO LEADER NEW CONN");
			xThread.start();
		}

		public void stop()
		{
			xRunning = false;
			try {
				xServerSocket.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

		public void waitDone()
		{
			try {
				xThread.join();
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void stopAndWait()
		{
			stop();
			waitDone();
		}

		public void run()
		{
			try {
				xRunning = true;
				xServerSocket = new ServerSocket(xPort);
				while(xRunning) {
					Socket peerSocket = xServerSocket.accept();
					CiceroLogger.logInfo("New Peer Connection From: " + peerSocket.getInetAddress().toString() + ":" + peerSocket.getPort());
					PeerMessageThread peerThread = new PeerMessageThread(peerSocket);
					xPeerConnections.add(peerThread);
					peerThread.start();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	class PeerMessageThread implements Runnable
	{
		private boolean xRunning;
		private Socket xPeerSocket;
		private Thread xThread;
		private OpenFlowMessageReader xFromPeerStream;

		public PeerMessageThread(Socket s)
		{
			xRunning = false;
			
			if(s != null) {
				xPeerSocket = s;
				try {
					xFromPeerStream = new OpenFlowMessageReader(s.getInputStream());
				} catch(IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}

		private void socketDisconnect(Socket sock)
		{
			try {
				System.out.println("DISCONNECTING FROM: " + sock);
				sock.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

		public void start() 
		{
			xRunning = true;
			xThread = new Thread(this);
			xThread.start();
		}

		public void stop()
		{
			socketDisconnect(xPeerSocket);
			xRunning = false;
		}

		public void waitDone()
		{
			try {
				xThread.join();
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void stopAndWait()
		{
			stop();
			waitDone();
		}

		public void run()
		{
			while(xRunning) {
				OpenFlowMessage ofMsg = xFromPeerStream.read();
				if(ofMsg == null) {
					stop();
					return;
				}
				OpenFlowBLSLeaderMessage leader_msg = new OpenFlowBLSLeaderMessage(ofMsg);
				CiceroOpenFlowBLSSignatureSet sig_set = xSignatureMap.get(leader_msg);
				synchronized(sig_set) {
					/*
					if(xRetiredXids.contains(leader_msg.getXID())) {
						return;
					}
					*/
					if(sig_set.addSignature(new CiceroBLSSignature(leader_msg.getNodeId(), leader_msg.getSignature())) >= xQuorumSize) {
						Element aggSignature = xBLSAggregator.aggregateSignatures(sig_set.getSignatures());
						xSwitchConnections.get(sig_set.getDpIndex()).sendSwitch(new OpenFlowBLSAggregateMessage(sig_set.getMessage(), aggSignature.toBytes()));
						
						/*
						if(xBLSVerifier.verify(sig_set.getMessage().getMessageBytes(), aggSignature)) {
							xRetiredXids.add(leader_msg.getXID());
							xSwitchConnections.get(sig_set.getDpIndex()).sendSwitch(new OpenFlowBLSAggregateMessage(sig_set.getMessage(), aggSignature.toBytes()));
							xSignatureMap.remove(leader_msg);
						}
						*/
					}
				}
			}
		}
	}
}
