/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.ReplicaContext;
import bftsmart.tom.ServiceProxy;
import bftsmart.tom.server.defaultservices.DefaultRecoverable;
import bftsmart.reconfiguration.views.View;

import java.nio.ByteBuffer;

import java.util.LinkedList;
import java.util.Map;
// import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

class CiceroQueueSettings {
	public static final boolean DEBUG = false;
};

/**
 * Cicero message queue replica that implements a BFT replicated event queue.
 *
 */

public class CiceroEventQueue
{
	private CiceroControllerLayer xLayerHandler;
	private CiceroQueueConsensusThread xConsensusThread;
	private CiceroQueueProposeThread xProposeThread;
	private CiceroQueueReceiveThread xReceiveThread;
	private boolean xShareProposalPending;
	private boolean xLogMetrics;
	
	private int xDKGPhase;
	private DKGBridge xDKGBridge;

	private int xId;
	private int xMaxBatch;
	private long xBatchTimeout;

	private CiceroWaterMarks xWaterMarks;
	private CiceroConfigProperties xConfig;

	// Consensus operations
	private static final byte CICERO_EVENT_OP = 1;
	private static final byte CICERO_START_SHARE = 2;
	private static final byte CICERO_DO_SHARE = 3;

	CiceroEventQueue(int id, CiceroConfigProperties props, CiceroControllerLayer hdlr)
	{
		xId = id;
		xMaxBatch = props.getMaxBatchSize();
		xBatchTimeout = props.getBatchTimeout();
		xLogMetrics = props.logMetrics();
		CiceroLogger.logInfo("xMatchBatch: " + xMaxBatch);
		CiceroLogger.logInfo("xBatchTimeout: " + xBatchTimeout);
		CiceroLogger.logInfo("xLogMetrics: " + xLogMetrics);
		xConfig = props;
		xLayerHandler = hdlr;
		xShareProposalPending = false;

		DKGSettings settings = props.getDKGSettings();
		if (settings != null) {
			xDKGPhase = props.getInitialDKGPhase();
			xDKGBridge = new DKGBridge(settings);
		} else {
			xDKGPhase = -1;
			xDKGBridge = null;
		}
			
		xWaterMarks = new CiceroWaterMarks();

		xConsensusThread = new CiceroQueueConsensusThread();
		xProposeThread = new CiceroQueueProposeThread();
		xReceiveThread = new CiceroQueueReceiveThread();
	}

	public void start()
	{
		new Thread(xConsensusThread, "BFT CONSENSUS").start();
		new Thread(xProposeThread, "BFT PROPOSE").start();
		new Thread(xReceiveThread ,"BFT RECEIVE").start();
	}

	public void enqueueEvent(CiceroEvent event)
	{
		xProposeThread.enqueueEvent(event);
	}

	public CiceroEvent dequeueEvent(int dp)
	{
		return xReceiveThread.getNextMessage(dp);
	}

	public List<CiceroEvent> dequeueEventBatch()
	{
		return xReceiveThread.getNextBatch();
	}

	public void sleepCatch(int ms)
	{
		try {
			TimeUnit.MILLISECONDS.sleep(ms);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	class CiceroWaterMarks
	{
		private List<Integer> xValues;
		// private Map< Long, Integer > xValues;

		CiceroWaterMarks()
		{
			xValues = Collections.synchronizedList(new ArrayList<Integer>());
			// xValues = new ConcurrentHashMap<Long, Integer>();
		}

		private void addToIndex(int index)
		{
			for(int i = xValues.size(); i <= index; i++) {
				xValues.add(-1);
			}
		}

		
		public int get(int index) 
		{
			if(index >= xValues.size()) {
				addToIndex(index);
			}
			return xValues.get(index);
		
			/*
			try {
				return xValues.get(index).intValue();
			}
			catch(NullPointerException e) {
				return 0;
			}
			*/
		}

		public void set(int index, int value)
		{
			if(index >= xValues.size()) {
				addToIndex(index);
			}
			xValues.set(index, value);

			//xValues.put(index, value);
		}
	}

	class CiceroOutputQueues
	{
		private List< BlockingQueue<CiceroEvent> > xValues;

		CiceroOutputQueues()
		{
			xValues = Collections.synchronizedList(new ArrayList< BlockingQueue<CiceroEvent> >());
		}

		private void addToIndex(int index)
		{
			for(int i = xValues.size(); i <= index; i++) {
				xValues.add(new LinkedBlockingQueue<CiceroEvent>());
			}
		}

		public CiceroEvent get(int index) 
		{
			if(index >= xValues.size()) {
				addToIndex(index);
			}
			try {
				return xValues.get(index).take();
			} catch(InterruptedException e) {
				e.printStackTrace();
				return null;
			}
		}

		public void put(int index, CiceroEvent value)
		{
			if(index >= xValues.size()) {
				addToIndex(index);
			}
			try {
				xValues.get(index).put(value);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		/*
		private Map< Long, BlockingQueue<CiceroEvent> > xValues;

		CiceroOutputQueues()
		{
			xValues = new ConcurrentHashMap< Long, BlockingQueue<CiceroEvent> >();
		}

		private BlockingQueue<CiceroEvent> getQueue(long index)
		{
			BlockingQueue<CiceroEvent> retVal = xValues.get(index);
			if(retVal == null) {
				retVal = xValues.put(index, new LinkedBlockingQueue<CiceroEvent>());
			}
			return retVal;
		}

		public CiceroEvent get(long index) 
		{
			try {
				return getQueue(index).take();
			} catch(InterruptedException e) {
				e.printStackTrace();
				return null;
			}
		}

		public void put(long index, CiceroEvent value)
		{
			try {
				getQueue(index).put(value);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		*/
	}

	class CiceroQueueConsensusThread extends DefaultRecoverable implements Runnable 
	{
		private BlockingQueue<byte[]> xEventQueue;
		private ServiceReplica xReplica = null;

		public CiceroQueueConsensusThread() 
		{
			xEventQueue = new LinkedBlockingQueue<byte[]>();
		}

		@Override
			public byte[][] appExecuteBatch(byte[][] commands, MessageContext[] msgCtxs, boolean fromConsensus)
			{    
				byte [][] replies = new byte[commands.length][];
				for (int i = 0; i < commands.length; i++) {
					if(msgCtxs != null && msgCtxs[i] != null) {
						replies[i] = executeSingle(commands[i],msgCtxs[i]);
					}
					else executeSingle(commands[i],null);
				}

				return replies;
			}

		@Override
			public byte[] appExecuteUnordered(byte[] command, MessageContext msgCtx) 
			{
				CiceroLogger.logError("appExecuteUndordered: should not get here");
				return new byte[1];
			}

		private boolean requestPhaseGood(int phase)
		{
			if(phase != xDKGPhase) {
				CiceroLogger.logError("Phase mismatch: " + phase + " != " + xDKGPhase);
				try {
					xDKGBridge.exitDKG();
				} catch(Exception e) {
					e.printStackTrace();
				}
				return false;
			}
			return true;
		}

		private byte[] executeSingle(byte[] command, MessageContext msgCtx) {
			ByteBuffer commandBuffer = ByteBuffer.wrap(command);
			byte commandOp = commandBuffer.get();
			switch(commandOp) {
				case CICERO_EVENT_OP:
					try {
						xEventQueue.put(command);
					} catch(InterruptedException e) {
						e.printStackTrace();
					}
					break;
				case CICERO_START_SHARE:
					// Can't start DKG if no replica object
					if(xDKGBridge != null && xReplica != null) {
						int phase = commandBuffer.getInt();
						if(requestPhaseGood(phase)) {
							ReplicaContext replicaCtx = xReplica.getReplicaContext();
							View currentView = replicaCtx.getCurrentView();
							int[] processes = currentView.getProcesses();
							String[] groupMembers = new String[processes.length];
							for(int i = 0; i < processes.length; i++) {
								groupMembers[i] = currentView.getAddress(processes[i]).getAddress().getHostAddress();
							}
							try {
								CiceroLogger.logInfo("starting share");
								xDKGBridge.startDKG(xDKGPhase, xConfig.getShare(), groupMembers);
							} catch (Exception e) {
								e.printStackTrace();
								try {
									xDKGBridge.exitDKG();
								} catch(Exception ex) {
									ex.printStackTrace();
									System.exit(1);
								}
							}
						}
					}
					break;
				case CICERO_DO_SHARE:
					// Can't start DKG if no replica object
					if(xDKGBridge != null && xReplica != null) {
						int phase = commandBuffer.getInt();
						if(requestPhaseGood(phase)) {
							try {
								// Only share if I already have one
								if (xConfig.getShare() != null) {
									xDKGBridge.startShare();
								}
								String newShare = xDKGBridge.waitForShare();
								xDKGPhase++;
								xConfig.setShare(newShare);
								xLayerHandler.shareChanged(newShare);
								CiceroLogger.logInfo("New Share: " + newShare + " New Phase: " + xDKGPhase);
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								xDKGBridge.exitDKG();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else {
						xDKGPhase++;
					}
					break;
				default:
					CiceroLogger.logWarn("Unknown consensus operation: " + command[0]);
					CiceroLogger.logDebug(Arrays.toString(command));
					break;
			}
			return new byte[1];
		}

		@SuppressWarnings("unchecked")
			@Override
			public void installSnapshot(byte[] state) {
				CiceroLogger.logDebug("setState called");
				if(xDKGBridge != null) {
					try {
						xDKGPhase = ByteBuffer.wrap(state).getInt();
						xConfig.setShare(null);
					} catch(Exception e) {
						e.printStackTrace();
						xDKGPhase = -1;
					}
				}
			}

		@Override
			public byte[] getSnapshot() {
				CiceroLogger.logDebug("getState called");
				if(xDKGBridge != null) {
					byte[] bytes = new byte[4];	
					ByteBuffer.wrap(bytes).putInt(xDKGPhase);
					return bytes;
				}
				return new byte[1];
			}

		public byte[] getOutputMessage() 
		{
			try {
				return xEventQueue.take();
			} catch(InterruptedException e) {
				return null;
			}
		}

		public ServiceReplica getReplica()
		{
			return xReplica;
		}

		public void run()
		{
			CiceroLogger.logInfo("Starting Cicero Consensus Thread...");
			xReplica = new ServiceReplica(xId, this, this);
			if(xConfig.getShare() == null) {
				CiceroLogger.logDebug("I HAVE NO SHARE");
				xProposeThread.initiateNewShare();
			}
		}
	}

	class CiceroProposeNewShareThread implements Runnable
	{
		public void run()
		{
			if (xShareProposalPending) {
				CiceroLogger.logWarn("Share proposal already pending, skipping");
				return;
			}
			xShareProposalPending = true;
			// sleepCatch(xConfig.getInitialShareDelay());
			while(xConsensusThread.getReplica() == null) {
				CiceroLogger.logWarn("No replica, sleeping...");
				sleepCatch(500);
			}
			while(xConsensusThread.getReplica().getReplicaContext() == null) {
				CiceroLogger.logWarn("No replica context, sleeping...");
				sleepCatch(500);
			}
			xProposeThread.proposeNewShare();
			xShareProposalPending = false;
		}
	}
	
	class CiceroQueueProposeThread implements Runnable
	{
		private BlockingQueue<CiceroEvent> xProposeQueue;
		private ServiceProxy xProxy;

		public CiceroQueueProposeThread()
		{
			xProposeQueue = new LinkedBlockingQueue<CiceroEvent>();
			xProxy = new ServiceProxy(xId);
		}

		public void initiateNewShare()
		{
			try {
				if(!xShareProposalPending) {
					new Thread(new CiceroProposeNewShareThread()).start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private byte[] buildPhaseMessage(byte type)
		{
			ByteBuffer buffer = ByteBuffer.allocate(5);
			buffer.put(type);
			buffer.putInt(xDKGPhase);
			return buffer.array();
		}

		private void timeShare()
		{
			long ts = System.currentTimeMillis();
			doProposeShareConsensus();
			long te = System.currentTimeMillis();
			CiceroLogger.logInfo("Share time: " + (te - ts));
		}

		private void doProposeShareConsensus()
		{
			// Propose start DKG process
			xProxy.invokeOrdered(buildPhaseMessage(CICERO_START_SHARE));

			// Propose do share
			xProxy.invokeOrdered(buildPhaseMessage(CICERO_DO_SHARE));
		}

		public void proposeNewShare() 
		{
			if(xConfig.useDKG() && xDKGBridge != null) {
				CiceroLogger.logInfo("Proposing new share");
				if(xLogMetrics) {
					timeShare();
				} else {
					doProposeShareConsensus();
				}
			}
		}

		public void enqueueEvent(CiceroEvent event)
		{
			try {
				if(xWaterMarks.get(event.getDpIndex()) < event.getId()) {
					xProposeQueue.put(event);
				}
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		private byte[] getProposeBuffer(CiceroEvent[] batch, int batch_length, int raw_size)
		{
			ByteBuffer data_buffer = ByteBuffer.allocate(raw_size + 1);
			data_buffer.put(CICERO_EVENT_OP);
			for(int i = 0; i < batch_length; i++) {
				data_buffer.put(batch[i].getData());
			}
			return data_buffer.array();
		}

		public void timeMeasure(int currentEventCount, long batchStartTime, byte[] proposeData)
		{
			CiceroLogger.logInfo("BATCH SIZE: " + currentEventCount + " BATCH TIME: " + (System.currentTimeMillis() - batchStartTime));
			long ts = System.currentTimeMillis();
			xProxy.invokeOrdered(proposeData);
			long te = System.currentTimeMillis();
			CiceroLogger.logInfo("CONSENSUS TIME: " + (te - ts));
		}

		public void run() 
		{
			CiceroLogger.logInfo("Starting Cicero Propose Thread...");

			while(true) {
				int currentEventCount = 0;
				long remainingTime = 0;
				int batchDataSize = 0;
				CiceroEvent[] proposeMessages = new CiceroEvent[xMaxBatch];
				try {
					proposeMessages[currentEventCount++] = xProposeQueue.take();
					batchDataSize += proposeMessages[currentEventCount-1].getRawSize();
				} catch (InterruptedException e) {
					continue;
				}
				long startTime = System.currentTimeMillis();
				try {
					while(currentEventCount < xMaxBatch) {
						remainingTime = startTime - System.currentTimeMillis() + xBatchTimeout;
						CiceroEvent nextEvent = xProposeQueue.poll(remainingTime, TimeUnit.MILLISECONDS);
						if(nextEvent == null) {
							// Batch timeout
							break;
						}
						proposeMessages[currentEventCount++] = nextEvent;
						batchDataSize += proposeMessages[currentEventCount-1].getRawSize();
					}
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
				if(xLogMetrics) {
					timeMeasure(currentEventCount, startTime, getProposeBuffer(proposeMessages, currentEventCount, batchDataSize));
				} else {
					xProxy.invokeOrdered(getProposeBuffer(proposeMessages, currentEventCount, batchDataSize));
				}
			}
		}
	}

	class CiceroQueueReceiveThread implements Runnable
	{
		private CiceroOutputQueues xOutputQueues;
		private List<CiceroEvent> xGlobalOutputQueue;

		public CiceroQueueReceiveThread() {
			xOutputQueues = new CiceroOutputQueues();
			xGlobalOutputQueue = new LinkedList<CiceroEvent>();
		}

		public CiceroEvent getNextMessage(int dp) {
			return xOutputQueues.get(dp);
		}

		public List<CiceroEvent> getNextBatch() {
			synchronized(xGlobalOutputQueue) {
				if (xGlobalOutputQueue.size() == 0) {
					while(true) {
						try {
							xGlobalOutputQueue.wait();
							break;
						} catch(InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				List<CiceroEvent> returnList = new LinkedList<CiceroEvent>(xGlobalOutputQueue);
				xGlobalOutputQueue.clear();
				return returnList;
			}
		}

		public void run()
		{
			CiceroLogger.logInfo("Starting Cicero Receive Thread...");

			while(true) {			
				byte[] msg = xConsensusThread.getOutputMessage();
				if(msg != null) {
					synchronized(xGlobalOutputQueue) {
						int offset = 1;
						ByteBuffer msg_buffer = ByteBuffer.wrap(msg);
						// Reemvoe the command operation
						msg_buffer.get();
						while(offset < msg.length) {
							int eventSize = msg_buffer.getInt();
							offset += (eventSize + 4);

							byte[] eventData = new byte[eventSize];
							msg_buffer.get(eventData);
							CiceroEvent event = new CiceroEvent(eventData);
							xGlobalOutputQueue.add(event);
							
							if(xWaterMarks.get(event.getDpIndex()) < event.getId()) {
								xWaterMarks.set(event.getDpIndex(), event.getId());
								xGlobalOutputQueue.add(event);
								// xOutputQueues.put(event.getDpIndex(), event);
							}/* else {
								CiceroLogger.logInfo(event.getDpIndex() + " Event: " + event.getId() + " less than watermark: " +
									xWaterMarks.get(event.getDpIndex()));
							}*/
						}
						if(xGlobalOutputQueue.size() > 0) {
							xGlobalOutputQueue.notify();
						}
					}
				}
			}
		}
	}
}
