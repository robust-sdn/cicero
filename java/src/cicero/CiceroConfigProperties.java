/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Cicero configuration properties
 *
 */

public class CiceroConfigProperties
{
	private int xId;
	private InputStream xPropInput;
	private Properties xCiceroProp;
	private String xShare;

	public CiceroConfigProperties()
	{
		xId = -1;
		xShare = null;
		try {
			String configFile = "cicero.config";
			xPropInput = getClass().getClassLoader().getResourceAsStream(configFile);
			if(xPropInput != null) {
				xCiceroProp = new Properties();
				xCiceroProp.load(xPropInput);
			} else {
				throw new FileNotFoundException("Config file '" + configFile + "' not found in the classpath");
			}
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public CiceroConfigProperties(int id)
	{
		this();
		if (id < 0) {
			throw new IllegalArgumentException("ID must >= 0");
		}
		xId = id;
		xShare = xCiceroProp.getProperty("SHARES." + (xId+1));
	}

	public int geId()
	{
		return xId;
	}

	public Properties getProperties()
	{
		return xCiceroProp;
	}

	public String getValue(String key)
	{
		String value = xCiceroProp.getProperty(key);
		if(value != null) {
			value = value.replaceAll("\\$\\{ID\\}", String.valueOf(xId));
		}
		return value;
	}

	public int getId()
	{
		return xId;
	}

	public String getShare()
	{
		return xShare;
	}

	public void setShare(String share)
	{
		xShare = share;
	}

	public boolean useDKG()
	{
		return xCiceroProp.getProperty("USEDKG", "false").equals("true");
	}

	public DKGSettings getDKGSettings()
	{
		if(useDKG()) {
			try {
				return new DKGSettings(xId+1, this);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getU()
	{
		return xCiceroProp.getProperty("U");
	}

	public String getPairingParamFile()
	{
		try {
			File pairing_file = new File(getClass().getClassLoader().getResource(xCiceroProp.getProperty("PAIRING")).toURI());
			return pairing_file.getAbsolutePath();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int getInitialDKGPhase()
	{
		return Integer.parseInt(xCiceroProp.getProperty("PHASE"));
	}

	public int getInitialShareDelay()
	{
		return Integer.parseInt(xCiceroProp.getProperty("SHAREDELAY"));
	}

	public boolean logMetrics()
	{
		return xCiceroProp.getProperty("LOGMETRICS", "false").equals("true");
	}

	public int getLogLevel()
	{
		return Integer.parseInt(xCiceroProp.getProperty("LOGLEVEL"));
	}

	public String getLogFile()
	{
		return getValue("LOGFILE");
	}

	public String getLogDir()
	{
		return getValue("LOGDIR");
	}

	public int getListenPort()
	{
		return Integer.parseInt(xCiceroProp.getProperty("LISTENPORT", "6633"));
	}

	public int getControllerPort()
	{
		return Integer.parseInt(xCiceroProp.getProperty("CONTROLLERPORT", "4000"));
	}

	public String getLeader()
	{
		return xCiceroProp.getProperty("LEADER");
	}

	public int getLeaderPort()
	{
		return Integer.parseInt(xCiceroProp.getProperty("LEADERPORT", "-1"));
	}

	public int getMaxBatchSize()
	{
		return Integer.parseInt(xCiceroProp.getProperty("MAXBATCH", "1000"));
	}

	public int getBatchTimeout()
	{
		return Integer.parseInt(xCiceroProp.getProperty("BATCHTIMEOUT", "10"));
	}

	public String getPublicKey()
	{
		return xCiceroProp.getProperty("PUBLICKEY");
	}

	public static void main(String[] args)
	{
		CiceroConfigProperties props = new CiceroConfigProperties(0);
		System.out.println(props.getDKGSettings().toString());
	}
}
