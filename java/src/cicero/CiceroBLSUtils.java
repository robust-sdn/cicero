/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.util.Set;
import java.util.Iterator;

import java.security.MessageDigest;
import it.unisa.dia.gas.jpbc.*;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

/**
 * Wrap verify a set of BLS signatures
 *
 */

class CiceroBLSSignature
{
	private int xNodeId;
	private byte[] xSignature;

	public CiceroBLSSignature(int n, byte[] s)
	{
		xNodeId = n;
		xSignature = s;
	}

	public int getNodeId()
	{
		return xNodeId;
	}

	public byte[] getSignature()
	{
		return xSignature;
	}
}

public class CiceroBLSUtils
{
	protected Pairing xPairing;
	protected Element xU;
	
	// Ensure the libjpbc-pbc is loaded
	static {
		System.load(System.getProperty("libjpbc.path"));
	}

	public CiceroBLSUtils(CiceroConfigProperties props)
	{
		PairingFactory.getInstance().setUsePBCWhenPossible(true);
		xPairing = PairingFactory.getPairing(props.getPairingParamFile());
		xU = xPairing.getG1().newElement();
		xU.setFromString(props.getU(), 10);
	}

	protected Element apply(Set<CiceroBLSSignature> sigs, int alpha)
	{
		Element falpha = xPairing.getG1().newOneElement();
		for(CiceroBLSSignature sigi : sigs) {
			int i = sigi.getNodeId();
			Element share = xPairing.getG1().newElementFromBytes(sigi.getSignature());
			Element t = xPairing.getZr().newElement(alpha);
			Element coeff = xPairing.getZr().newOneElement();
			for(CiceroBLSSignature sigj : sigs) {
				int j = sigj.getNodeId();
				if(j == i) continue;
				t.set(j - alpha);
				coeff.mul(t);
				t.set(j - i);
				t.invert();
				coeff.mul(t);
			}
			share.powZn(coeff);
			falpha.mul(share);
		}
		return falpha;
	}

	protected Element computeMessageHash(byte[] message) throws Throwable
	{
		MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
		byte[] msgHash = msdDigest.digest(message);
		return xPairing.getG1().newElementFromHash(msgHash, 0, msgHash.length);
	}
}

class CiceroBLSAggregator extends CiceroBLSUtils
{
	public CiceroBLSAggregator(CiceroConfigProperties props)
	{
		super(props);
	}

	public Element aggregateSignatures(Set<CiceroBLSSignature> sigShares)
	{
		return xPairing.pairing(xU, apply(sigShares, 0));
	}

	public byte[] aggregateSignaturesToBytes(Set<CiceroBLSSignature> sigShares)
	{
		return aggregateSignatures(sigShares).toBytes();
	}
}

class CiceroBLSVerifier extends CiceroBLSAggregator
{
	private Element xPublicKey;

	public CiceroBLSVerifier(CiceroConfigProperties props)
	{
		super(props);
		xPublicKey = xPairing.getG1().newElement();
		xPublicKey.setFromString(props.getPublicKey(), 10);
	}

	private Element computeMessageSignature(byte[] message)
	{
		try{
			return xPairing.pairing(xPublicKey, computeMessageHash(message));
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean verify(byte[] message, Element aggSignature)
	{
		Element msgSignature = computeMessageSignature(message);
		return aggSignature.isEqual(msgSignature);
	}

	public boolean verify(byte[] message, Set<CiceroBLSSignature> sigShares)
	{
		Element msgSignature = computeMessageSignature(message);
		Element aggSignature = aggregateSignatures(sigShares);
		return aggSignature.isEqual(msgSignature);
	}
}

class CiceroBLSSigner extends CiceroBLSUtils
{
	private Element xShare;
	
	public CiceroBLSSigner(CiceroConfigProperties props)
	{
		super(props);
		xShare = null;
		setShare(props.getShare());
	}

	public Element sign(byte[] message)
	{
		try {
			//System.out.println("Signing my share: " + xShare);
			return computeMessageHash(message).powZn(xShare);
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public byte[] signToBytes(byte[] message)
	{
		return sign(message).toBytes();
	}

	public void setShare(String share)
	{
		if(share == null || share.length() == 0) {
			return;
		}
		if(xShare == null) {
			xShare = xPairing.getZr().newElement();
		}
		xShare.setFromString(share, 10);
	}
}

