/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package cicero;

import java.io.IOException;
import java.io.InputStream;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

/**
 * OpenFlowMessageReader - Reads OpenFlowMessages from the given input stream
 *
 */

public class OpenFlowMessageReader
{
	private InputStream xInputStream;
	private byte[] xHeaderBuffer;

	public OpenFlowMessageReader(InputStream str)
	{
		xInputStream = str;
		xHeaderBuffer = new byte[OpenFlowMessage.OF_HEADER_SIZE];
	}

	public synchronized OpenFlowMessage read()
	{
		try {
			short msg_size = 0;
			while(msg_size == 0) {
				readWait(xHeaderBuffer);
				msg_size = ByteBuffer.wrap(xHeaderBuffer, 2, 2).getShort();
			}
			ByteBuffer msgBuffer = ByteBuffer.allocate(msg_size);
			msgBuffer.put(xHeaderBuffer);
			if(msg_size > OpenFlowMessage.OF_HEADER_SIZE) {
				readWait(msgBuffer.array(), OpenFlowMessage.OF_HEADER_SIZE, msg_size - OpenFlowMessage.OF_HEADER_SIZE);
			}
			msgBuffer.rewind();
			return new OpenFlowMessage(msgBuffer);
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void readWait(byte[] dest, int offset, int len) throws IOException
	{
		int remaining_to_read = len;
		int dest_offset = offset;
		while(remaining_to_read > 0) {
			dest_offset = xInputStream.read(dest, dest_offset, remaining_to_read);
			if(dest_offset < 0) {
				throw new IOException("Stream EOF");
			}
			remaining_to_read -= dest_offset;
		}
	}

	private void readWait(byte[] dest) throws IOException
	{
		readWait(dest, 0, dest.length);
	}
}
