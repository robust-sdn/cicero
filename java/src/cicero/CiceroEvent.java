/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.nio.ByteBuffer;

/**
 * Cicero event structure
 *
 */

public class CiceroEvent
{
	private int xId;
	private int xDpIndex;
	private OpenFlowMessage xMsg;

	CiceroEvent(int id, int dp, OpenFlowMessage msg)
	{
		xId = id;
		xDpIndex = dp;
		xMsg = msg;
	}

	CiceroEvent(byte[] rawData)
	{
		ByteBuffer data_buffer = ByteBuffer.wrap(rawData);
		xId = data_buffer.getInt();
		xDpIndex = data_buffer.getInt();
		xMsg = new OpenFlowMessage(data_buffer);
	}

	public final int getId()
	{
		return xId;
	}

	public final int getDpIndex()
	{
		return xDpIndex;
	}

	public OpenFlowMessage getMessage()
	{
		return xMsg;
	}

	public final int getRawSize()
	{
		return 12 + xMsg.getSize();
	}

	public byte[] getData()
	{
		ByteBuffer data_buffer = ByteBuffer.allocate(12 + xMsg.xSize);
		data_buffer.putInt(xMsg.xSize + 8);
		data_buffer.putInt(xId);
		data_buffer.putInt(xDpIndex);
		data_buffer.put(xMsg.getMessageBytes());
		return data_buffer.array();
	}
}
