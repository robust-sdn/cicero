/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cicero;

import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * OpenFlow Message Handler Cicero Controller Specific
 *
 */

public class CiceroControllerLayer
{
	private int xNodeId;
	private CiceroEventQueue xEventQueue;
	private EventQueueThread xEventThread;
	private NewSwitchConnectionThread xSwitchConnectionThread;
	private List<ProxyConnection> xConnections;
	private List<CiceroAckSeq> xReceivedAcks;
	private List<SwitchUpdateThread> xUpdateThreads;
	private CiceroBLSSigner xBLSSigner;
	// private CiceroXIDManager xXIDManager;

	private CiceroLeaderLayer xLeaderLayer;
	private CiceroLeaderConnection xLeaderConnection;
	private boolean xUsingLeader;
	private String xLeaderHost;
	private int xLeaderPort;
		
	private static final int xMinCiceroXID = 0x80000000;

	private static final Set<Byte> CICERO_REPLY_TYPES = 
		new HashSet<Byte>(
			// TODO: ADD ALL APPROPRIATE MESSAGE TYPES
			Arrays.asList(
				OpenFlowMessage.OF_ROLEREQUEST,
				OpenFlowMessage.OF_PACKETOUT,
				OpenFlowMessage.OF_FLOWMOD
			)
		);

	public CiceroControllerLayer(int id, CiceroConfigProperties props)
	{
		xNodeId = id;
		xEventQueue = new CiceroEventQueue(id, props, this);
		xEventThread = new EventQueueThread();
		xSwitchConnectionThread = new NewSwitchConnectionThread(props.getListenPort(), props.getControllerPort());
		xConnections = new ArrayList<ProxyConnection>();
		xReceivedAcks = new ArrayList<CiceroAckSeq>();
		xUpdateThreads = new ArrayList<SwitchUpdateThread>();
		// xXIDManager = new CiceroXIDManager();
		xBLSSigner = null;
		xLeaderLayer = null;
		xUsingLeader = false;
		if (props.useDKG()) {
			CiceroLogger.logInfo("Node: " + id + " - Using DKG and BLS");
			xBLSSigner = new CiceroBLSSigner(props);
			xLeaderHost = props.getLeader();
			xLeaderPort = props.getLeaderPort();
			if(xLeaderHost != null && xLeaderPort != -1) {
				xUsingLeader = true;
				xLeaderLayer = new CiceroLeaderLayer(props, xConnections);
			}
		}
	}

	public void start()
	{
		if(xUsingLeader) {
			CiceroLogger.logInfo("Node: " + xNodeId + " - Using Leader Based Aggregation");
			xLeaderLayer.start();
			xLeaderConnection = new CiceroLeaderConnection(xLeaderHost, xLeaderPort);
			xLeaderConnection.connect();
		}
		
		Thread runEventThread = new Thread(xEventThread, "CICERO Event");
		Thread runNewConnectionThread = new Thread(xSwitchConnectionThread, "CICERO NEW CON");

		xEventQueue.start();
		runEventThread.start();
		runNewConnectionThread.start();

		if(xUsingLeader) {
			xLeaderLayer.waitDone();
		}
		try {
			runEventThread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		try {
			runNewConnectionThread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean isCiceroReply(OpenFlowMessage ofMsg)
	{
		return CICERO_REPLY_TYPES.contains(ofMsg.getType());
	}

	private boolean isCiceroEvent(OpenFlowMessage ofMsg)
	{
		return ofMsg.getType() == OpenFlowMessage.OF_PACKETIN;
	}

	private boolean isCiceroAck(OpenFlowMessage ofMsg)
	{
		// Cicero ACKS have an XID < 0
		if(ofMsg.getXID() >= 0) {
			return false;
		}

		if(ofMsg.getVersion() == 1 && ofMsg.getType() == OpenFlowMessage.OF_10_BARRIERRESPONSE) {
			return true;
		}
		if(ofMsg.getVersion() > 1 && ofMsg.getType() == OpenFlowMessage.OF_11_BARRIERRESPONSE) {
			return true;
		}
		return false;
	}

	/*
	private ProxyConnection getConnectionForDpId(long dpid)
	{
		while(true) {
			try {
				Iterator<ProxyConnection> iterator = xConnections.iterator();
				while(iterator.hasNext()) {
					ProxyConnection conn = iterator.next();
					if(conn.getDpId() == dpid) {
						return conn;
					}
				}
			} catch(Exception ex) {
				// Intentinally left blank
			}
			Thread.yield();
		}
	}
	*/

	public void shareChanged(String newShare)
	{

	}

	class CiceroAckSeq
	{
		private long xSeqValue;
		
		public CiceroAckSeq()
		{
			xSeqValue = Integer.toUnsignedLong(xMinCiceroXID) - 1;
		}

		public synchronized void checkValueAndWait(long value)
		{
			while(xSeqValue < value) {
				// System.out.println("F CHECK DEP: " + xSeqValue + " " + value);
				try {
					wait();
					//System.out.println("WAKE: " + xSeqValue + " " + value);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			// System.out.println("T CHECK DEP: " + xSeqValue + " " + value);
		}

		public synchronized void setValueAndNotify(long value)
		{
			if(value == xSeqValue + 1) {
				xSeqValue = value;
				notifyAll();
			}
		}
	}

	/*
	class CiceroXIDManager
	{
		private Map<Long, AtomicInteger> xXIDMap;

		CiceroXIDManager()
		{
			xXIDMap = new HashMap<Long, AtomicInteger>();
		}

		public int getNextXid(long dpid)
		{
			return getInternalNextXid(dpid) | xMinCiceroXID;
		}

		private int getInternalNextXid(long dpid)
		{
			try {
				return xXIDMap.get(dpid).getAndIncrement() & 0x7FFFFFFF;
			}
			catch(NullPointerException ex) {
				synchronized(xXIDMap) {
					xXIDMap.put(dpid, new AtomicInteger(0));
					return xXIDMap.get(dpid).getAndIncrement() & 0x7FFFFFFF;
				}
			}
		}
	}
	*/

	class CiceroProxyHandler extends OpenFlowProxyHandler
	{
		private int xIndex;
		private boolean xRoleSent;

		CiceroProxyHandler(int idx)
		{
			xIndex = idx;
			xRoleSent = false;
		}
	
		public void handleNewConnection(ProxyConnection conn)
		{
			if(xUsingLeader) {
			}
		}
		public void handleClosedConnection(ProxyConnection conn) {}

		private int getNextCiceroXID()
		{
			return getNextXID() | xMinCiceroXID; 
		}

	
		public synchronized void handleOpenFlowMessageFromController(ProxyConnection conn, OpenFlowMessage ofMsg)
		{
			if(!isCiceroReply(ofMsg)) {
				conn.sendSwitch(ofMsg);
				return;
			}
			// long dpid = conn.getDpId();
			// int msg_xid = xXIDManager.getNextXid(dpid);
			int msg_xid = getNextCiceroXID();
			// System.out.println(dpid + " SEND SWITCH: " + ofMsg + " NEWXID: " + msg_xid);
			ofMsg.setXID(msg_xid);
			handlePolicyReceived(xIndex, conn, ofMsg);
		}

		public synchronized void handleOpenFlowMessageFromSwitch(ProxyConnection conn, OpenFlowMessage ofMsg)
		{
			if(isCiceroEvent(ofMsg)) {
				//long dpid = conn.getDpId();
				// System.out.println(xIndex + " " + dpid + " RECVS SWITCH: " + ofMsg);
				xEventQueue.enqueueEvent(new CiceroEvent(ofMsg.getXID(), xIndex, ofMsg));
			} else if(isCiceroAck(ofMsg)) {
				// long dpid = conn.getDpId();
				//System.out.println(dpid + " RECVS SWITCH: " + ofMsg);
				handleAckReceived(xIndex, conn, ofMsg);
			} else {
				conn.sendController(ofMsg);
				if(xUsingLeader && !xRoleSent && conn.getNegotiatedVersion() >= OpenFlowMessage.OF_VERSION_12) {
					CiceroLogger.logInfo("Sending ROLE REQUEST: " + conn);
					// int msg_xid = xXIDManager.getNextXid(conn.getDpId());
					int msg_xid = getNextCiceroXID();
					OpenFlowRoleRequestMessage role_msg = new OpenFlowRoleRequestMessage(conn.getNegotiatedVersion(), msg_xid, OpenFlowRoleRequestMessage.OF_ROLE_MASTER, msg_xid);
					conn.sendSwitch(role_msg);
					xRoleSent = true;
				}
			}
		}
	}

	class EventQueueThread implements Runnable
	{
		private boolean xRunning;

		EventQueueThread()
		{
			xRunning = false;
		}

		public void stop()
		{
			xRunning = false;
		}

		public void run()
		{
			xRunning = true;
			while(xRunning) {
				List<CiceroEvent> eventBatch = xEventQueue.dequeueEventBatch();
				Iterator<CiceroEvent> iterator = eventBatch.iterator();
		  		while(iterator.hasNext()) {
					CiceroEvent event = iterator.next();
					xConnections.get(event.getDpIndex()).sendController(event.getMessage());
				}
				//System.out.println("EV: " + event.getMessage().toString());
			}
		}
	}

	class NewSwitchConnectionThread implements Runnable
	{
		private int xPort;
		private int xControllerPort;
		private boolean xRunning;

		NewSwitchConnectionThread(int port, int ctl_port)
		{
			xPort = port;
			xControllerPort = ctl_port;
			xRunning = false;
		}

		public void stop()
		{
			xRunning = false;
		}

		public void run()
		{
			try {
				xRunning = true;
				int current_dp = 0;
				ServerSocket serverSocket = new ServerSocket(xPort);
				while(xRunning) {
					Socket switchSocket = serverSocket.accept();
					CiceroLogger.logInfo("New Connection From: " + switchSocket.getInetAddress().toString() + ":" + switchSocket.getPort());
	
					Socket controllerSocket = new Socket("localhost", xControllerPort);
					OpenFlowProxyHandler ofHandler = new CiceroProxyHandler(current_dp);
					ProxyConnection conn = null;
					if(xUsingLeader) {
						conn = new CiceroBLSLeaderConnection(current_dp, switchSocket, controllerSocket, xLeaderConnection, ofHandler, xNodeId+1, xBLSSigner);
						xLeaderLayer.addDataPath(current_dp);
					} else if(xBLSSigner != null) {
						conn = new CiceroBLSConnection(current_dp, switchSocket, controllerSocket, ofHandler, xNodeId+1, xBLSSigner);
					} else {
						conn = new ProxyConnection(current_dp, switchSocket, controllerSocket, ofHandler);
					}
					SwitchUpdateThread updateThread = new SwitchUpdateThread();
					xConnections.add(current_dp, conn);
					xReceivedAcks.add(current_dp, new CiceroAckSeq());
					xUpdateThreads.add(current_dp, updateThread);
					conn.start();
					updateThread.start();
					current_dp++;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	class PolicyUpdateThread implements Runnable
	{
		CiceroPolicy xPolicy;
		boolean xRunning;

		public PolicyUpdateThread(CiceroPolicy p)
		{
			xPolicy = p;
			xRunning = false;
		}

		public void start() 
		{
			xRunning = true;
			new Thread(this).start();
		}

		public void stop()
		{
			xRunning = false;
		}

		private void waitDepFullfilled(CiceroRuleDependence dep)
		{
			if (dep == null) {
				return;
			}
			xReceivedAcks.get(dep.getDpIndex()).checkValueAndWait(dep.getXID());
		}

		private void waitToSendRule(CiceroRule rule)
		{
			waitDepFullfilled(rule.getFlowDep());
			waitDepFullfilled(rule.getSwitchDep());
		}

		public void run()
		{
			try {
				while(xRunning) {
					CiceroRule nextRule = xPolicy.getNextRule();
					waitToSendRule(nextRule);
					xConnections.get(nextRule.getDpIndex()).sendSwitch(nextRule.getMessage());
					xPolicy.pullRule();
					if (xPolicy.isEmpty()) {
						return;
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	class SwitchUpdateThread implements Runnable
	{
		private BlockingQueue<CiceroRule> xRuleQueue;
		boolean xRunning;

		public SwitchUpdateThread()
		{
			xRunning = false;
			xRuleQueue = new LinkedBlockingQueue<CiceroRule>();
		}
		
		public void start() 
		{
			xRunning = true;
			new Thread(this).start();
		}

		public void stop()
		{
			xRunning = false;
		}

		private void waitDepFullfilled(CiceroRuleDependence dep)
		{
			if (dep == null) {
				return;
			}
			xReceivedAcks.get(dep.getDpIndex()).checkValueAndWait(dep.getXID());
		}

		private void waitToSendRule(CiceroRule rule)
		{
			waitDepFullfilled(rule.getFlowDep());
			waitDepFullfilled(rule.getSwitchDep());
		}

		public void addRule(CiceroRule r)
		{
			try {
				xRuleQueue.put(r);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void run()
		{
			while(xRunning) {
				try {
					CiceroRule nextRule = xRuleQueue.take();
					waitToSendRule(nextRule);
					xConnections.get(nextRule.getDpIndex()).sendSwitch(nextRule.getMessage());
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void handleAckReceived(int index, ProxyConnection conn, OpenFlowMessage ofMsg)
	{
		//System.out.println("ACK RECV: " + Integer.toUnsignedLong(ofMsg.getXID()));
		xReceivedAcks.get(index).setValueAndNotify(Integer.toUnsignedLong(ofMsg.getXID()));
	}
	
	public void handlePolicyReceived(int index, ProxyConnection conn, OpenFlowMessage ofMsg)
	{
		synchronized(xReceivedAcks) {
			//System.out.println("Policy Recieved: " + index + " " + ofMsg);
			//CiceroPolicy newPolicy = new CiceroPolicy();
			//CiceroRule newRule = new CiceroRule(index, ofMsg, null, new CiceroRuleDependence(index, Integer.toUnsignedLong(ofMsg.getXID()) - 1));
			//newPolicy.pushRule(newRule);
			//new Thread(new PolicyUpdateThread(newPolicy), "Policy: " + Integer.toUnsignedLong(ofMsg.getXID())).start();
			//CiceroRule newRule = new CiceroRule(index, ofMsg, null, new CiceroRuleDependence(index, Integer.toUnsignedLong(ofMsg.getXID()) - 1));
			CiceroRule newRule = new CiceroRule(index, ofMsg, null, null);
			xUpdateThreads.get(index).addRule(newRule);
		}
	}
}
