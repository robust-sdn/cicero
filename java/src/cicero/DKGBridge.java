/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package cicero;

import java.io.File;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.Properties;

import java.lang.reflect.Field;

/**
 * Distributed Key Generation command bridge
 *
 */

class DKGUtils
{
	public static String getCertFileFromId(String certs_path, int id)
	{
		return certs_path + "/" + id + ".pem";
	}

	public static String getKeyFileFromId(String certs_path, int id)
	{
		return certs_path + "/" + id + "-key.pem";
	}
}

class DKGSettings
{
	public String xCommandPath;
	public String xLogDir;
	public String xMsgLog;
	public String xErrLog;
	public String xTimeoutLog;
	public int xId;
	public int xPort;
	public int xT;
	public int xF;
	public String xU;
	public String xPairingParamPath;
	public String xCertsPath;

	public DKGSettings(int id, CiceroConfigProperties props) throws Exception
	{
		xId = id;
		Properties config = props.getProperties();
		xCommandPath = config.getProperty("DKGCMD");
		xLogDir = config.getProperty("LOGDIR");
		xMsgLog = config.getProperty("DKGMSGLOG");
		xErrLog = config.getProperty("DKGERRLOG");
		xTimeoutLog = config.getProperty("DKGTIMEOUTLOG");
		xPort = Integer.parseInt(config.getProperty("DKGPORT"));
		xT = Integer.parseInt(config.getProperty("T"));
		xF = Integer.parseInt(config.getProperty("F"));
		xU = config.getProperty("U");
		File pairing_file = new File(getClass().getClassLoader().getResource(config.getProperty("PAIRING")).toURI());
		xPairingParamPath = pairing_file.getAbsolutePath();
		xCertsPath = config.getProperty("CERTS"); 
	}

	public String toString()
	{
		try {
			Field[] classFields = getClass().getFields();
			StringBuffer values = new StringBuffer();
			for(int i = 0; i < classFields.length; i++) {
				values.append(classFields[i].getName());
				values.append(": ");
				Object fieldValue = classFields[i].get(this);
				values.append(fieldValue != null ? fieldValue.toString() : "null");
				if(i != classFields.length-1) {
					values.append("\n");
				}
			}
			return values.toString();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

abstract class TempFile
{
	protected File xTempFile;

	protected TempFile(String baseName)
	{
		try {
			xTempFile = File.createTempFile(baseName, null);
			xTempFile.deleteOnExit();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	protected PrintWriter getPrintWriter()
	{
		try {
			return new PrintWriter(new BufferedWriter(new FileWriter(xTempFile)));
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	public String getPath()
	{
		return xTempFile.getPath();
	}

	public void delete()
	{
		try {
			xTempFile.delete();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}

class ContactList extends TempFile
{
	public ContactList(String[] contact_ips, int port, String certs_path)
	{
		super("cicero-dkg-contlist");
		PrintWriter contactWriter = getPrintWriter();
		for(int i = 0; i < contact_ips.length; i++) {
			int id = i+1;
			contactWriter.println(
				id + " " +
				contact_ips[i] + " " +
				port + " " +
				DKGUtils.getCertFileFromId(certs_path, id));
		}
		contactWriter.flush();
		contactWriter.close();
	}
}

class SystemParam extends TempFile
{
	public SystemParam(int n, int t, int f, String U)
	{
		super("cicero-dkg-sysparam");
		PrintWriter sysParamWriter = getPrintWriter();
		sysParamWriter.println("n " + n);
		sysParamWriter.println("t " + t);
		sysParamWriter.println("f " + f);
		sysParamWriter.println("phaseDuration 15");
		sysParamWriter.println("U " + U);
		sysParamWriter.flush();
		sysParamWriter.close();
	}
}

public class DKGBridge
{
	private static final long CMD_WAIT_TIME = 100;
	private static final TimeUnit CMD_WAIT_UNITS = TimeUnit.MILLISECONDS;
	private static final String MESSAGE_LOG = "/dev/null";
	private static final String TIMEOUT_LOG = "/dev/null";
	private static final String ERROR_LOG = "/dev/null";
	private static final String TIMEOUT_VALUE = "/dev/null";
	private static final String CERTS_BASE_DIR = "/";

	private ExternalCommand xDKGNodeCommand;
	private ContactList xContactList;
	private SystemParam xSystemParam;
	private DKGSettings xSettings;

	public DKGBridge(DKGSettings settings)
	{
		xDKGNodeCommand = null;
		xSettings = settings;
		CiceroLogger.logInfo("Initiating DKG\n" + xSettings.toString());
	}

	private void reset()
	{
		if(xContactList != null) {
			xContactList.delete();
			xContactList = null;
		}
		if (xSystemParam != null) {
			xSystemParam.delete();
			xSystemParam = null;
		}
		if(xDKGNodeCommand != null) {
			if(xDKGNodeCommand.commandAlive()) {
				xDKGNodeCommand.terminateCommand(CMD_WAIT_TIME, CMD_WAIT_UNITS, ExternalCommand.FORCIBLY_NO);
			}
			xDKGNodeCommand = null;
		}
	}

	private void checkErrors() throws RuntimeException
	{
		String errors = xDKGNodeCommand.readAllFromStdErrOrExit();
		if(errors.length() > 0) {
			throw new RuntimeException("DKG node error:\n" + errors.trim());
		}
	}

	public void startDKG(int phase, String share, String[] contact_ips) throws IOException
	{
		reset();
		File logDir = new File(xSettings.xLogDir);
		logDir.mkdirs();
		String msgLogPath = xSettings.xMsgLog != null ? xSettings.xLogDir + "/" + xSettings.xMsgLog : MESSAGE_LOG;
		String errLogPath = xSettings.xErrLog != null ? xSettings.xLogDir + "/" + xSettings.xErrLog : ERROR_LOG;
		String timeoutLogPath = xSettings.xTimeoutLog != null ? xSettings.xLogDir + "/" + xSettings.xTimeoutLog : TIMEOUT_LOG;

		xContactList = new ContactList(contact_ips, xSettings.xPort, xSettings.xCertsPath);
		xSystemParam = new SystemParam(contact_ips.length, xSettings.xT, xSettings.xF, xSettings.xU);
		String[] cmdArgs = {
			"-a", xSettings.xPairingParamPath,
			"-s", xSystemParam.getPath(),
			"-d", CERTS_BASE_DIR,
			"-m", msgLogPath,
			"-i", timeoutLogPath,
			"-g", errLogPath,
			"-h", String.valueOf(phase),
			"-v", TIMEOUT_VALUE,
			"-p", String.valueOf(xSettings.xPort),
			DKGUtils.getCertFileFromId(xSettings.xCertsPath, xSettings.xId),
			DKGUtils.getKeyFileFromId(xSettings.xCertsPath, xSettings.xId),
			xContactList.getPath(),
			"-e", share,
		};
		if(share == null) {
			xDKGNodeCommand = new ExternalCommand(xSettings.xCommandPath, Arrays.copyOf(cmdArgs, cmdArgs.length-2));
		} else {
			xDKGNodeCommand = new ExternalCommand(xSettings.xCommandPath, cmdArgs);
		}
		xDKGNodeCommand.runCommand();
		checkErrors();
	}

	private void sendDKGCommand(String cmd) throws IOException,InterruptedException
	{
		xDKGNodeCommand.writeToStdIn(cmd + "\n");
		Thread.sleep(CMD_WAIT_TIME, 0);
		checkErrors();
	}

	public void exitDKG() throws IOException,InterruptedException
	{
		sendDKGCommand("exit");
		reset();
	}

	public void startShare() throws IOException,InterruptedException
	{
		sendDKGCommand("share");
	}

	public String waitForShare() throws IOException,InterruptedException
	{
		String prefix = "Share is : ";
		String line = xDKGNodeCommand.readLineFromStdOut();
		while(line != null) {
			CiceroLogger.logDebug(line);
			if(line.startsWith(prefix)) {
				return line.substring(prefix.length());
			}
			line = xDKGNodeCommand.readLineFromStdOut();
		}
		return null;
	}

	public static void main(String[] args) throws Exception 
	{
		int node = Integer.parseInt(args[0]);
		int phase = Integer.parseInt(args[1]);
	
		CiceroConfigProperties ciceroConfig = new CiceroConfigProperties(node);
		DKGSettings settings = ciceroConfig.getDKGSettings();
		System.out.println(settings.toString());
		DKGBridge bridge = new DKGBridge(settings);
		String[] contact_ips = {"10.1.1.2", "10.1.1.3", "10.1.1.4", "10.1.1.5"};
		bridge.startDKG(phase, null, contact_ips);
		if(node <= settings.xT + 1) {
			TimeUnit.SECONDS.sleep(2);
			bridge.startShare();
		}
		System.out.println(bridge.waitForShare());
		bridge.exitDKG();
	}
}
