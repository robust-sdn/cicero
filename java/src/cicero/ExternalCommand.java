/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package cicero;

import java.io.Reader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * External Command
 *
 */

public class ExternalCommand
{
	public static final boolean FORCIBLY_NO = false;
	public static final boolean FORCIBLY_YES = true;

	private String xCommandString;
	private String[] xCommandArgs;

	private Process xProcess;
	private InputStream xStdOut;
	private BufferedReader xStdOutReader;
	private InputStream xStdErr;
	private BufferedReader xStdErrReader;
	private OutputStream xStdIn;

	public ExternalCommand(String cmd)
	{
		xCommandString = cmd;
		xCommandArgs = null;
		xProcess = null;
	}

	public ExternalCommand(String cmd, String[] args)
	{
		xCommandString = cmd;
		xCommandArgs = args;
		xProcess = null;
	}

	private void trySleep(long millis, int nanos)
	{
		try {
			Thread.sleep(millis, nanos);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public boolean commandAlive()
	{
		if(xProcess != null) {
			return xProcess.isAlive();
		}
		return false;
	}

	public void runCommand() throws IOException, RuntimeException
	{
		if(xCommandArgs != null) {
			String[] xCommandArray = new String[xCommandArgs.length + 1];
			System.arraycopy(xCommandArgs, 0, xCommandArray, 1, xCommandArgs.length);
			xCommandArray[0] = xCommandString;
			xProcess = Runtime.getRuntime().exec(xCommandArray);
			CiceroLogger.logDebug(String.join(" ", xCommandArray));
		} else {
			xProcess = Runtime.getRuntime().exec(xCommandString);
		}
		if(xProcess != null) {
			trySleep(100, 0);
			xStdIn = xProcess.getOutputStream();
			xStdOut = xProcess.getInputStream();
			xStdOutReader = new BufferedReader(new InputStreamReader(xStdOut));
			xStdErr = xProcess.getErrorStream();
			xStdErrReader = new BufferedReader(new InputStreamReader(xStdErr));
			if(!xProcess.isAlive()) {
				String failure_message = "Command Execution Failed (Exit Code: " + xProcess.exitValue() + ")";
				try {
					int std_err_available = xStdErr.available();
					if(std_err_available > 0) {
						failure_message = failure_message + "\n" + readFromStdErr(std_err_available, false);
					}
				} catch(Exception e) {}
				throw new RuntimeException(failure_message);
			}
		}
	}

	public void runCommandOrExit()
	{
		try {
			runCommand();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void closeStreamContinue(Closeable stream)
	{
		try {
			if(stream != null) {
				stream.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void closeStreams()
	{
		if(xStdIn != null) {
			closeStreamContinue(xStdIn);
			xStdIn = null;
		}
		if(xStdOut != null) {
			closeStreamContinue(xStdOut);
			xStdOut = null;
		}
		if(xStdOutReader != null) {
			closeStreamContinue(xStdOutReader);
			xStdOutReader = null;
		}
		if(xStdErr != null) {
			closeStreamContinue(xStdErr);
			xStdErr = null;
		}
		if(xStdErrReader != null) {
			closeStreamContinue(xStdErrReader);
			xStdErrReader = null;
		}
	}

	public int waitForCommandOrTerminate(long timeout, TimeUnit unit)
	{
		// Close data streams
		closeStreams();
		
		// Wait for the process to die
		try {
			if(xProcess.waitFor(timeout, unit) == false) {

				// Process did not die, force terminate
				return terminateForcibly();
			}
			return xProcess.exitValue();
		} catch(Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int terminateForcibly()
	{
		// Force terminate immediately (no timeout)
		return terminateCommand(0, TimeUnit.NANOSECONDS, true);
	}

	public int terminateCommand(long timeout, TimeUnit unit, boolean forcibly)
	{
		// Close data streams
		closeStreams();
		
		// Kill the process
		try {
			if(forcibly) {
				xProcess.destroyForcibly();
			} else {
				xProcess.destroy();
			}
			if(timeout > 0) {
				if(xProcess.waitFor(timeout, unit) == false) {
					// Process did not die, force terminate
					//    if the process was already force
					//    terminated then don't do it again
					if(!forcibly) {
						xProcess.destroyForcibly();
					}
				}
			}
			return xProcess.exitValue();
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	private void writeToOutputStream(OutputStream stream, String data) throws IOException
	{
		stream.write(data.getBytes());
		stream.flush();
	}

	private void writeToOutputStreamOrExit(OutputStream stream, String data)
	{
		try {
			writeToOutputStream(stream, data);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void writeToStdIn(String data) throws IOException
	{
		writeToOutputStream(xStdIn, data);
	}
	public void writeToStdInOrExit(String data)
	{
		writeToOutputStreamOrExit(xStdIn, data);
	}

	private String readFromInputStream(InputStream stream, int count, boolean block) throws IOException
	{
		String retString;
		byte[] readBuffer;
		int bytesToRead = count;
		if(!block) {
			int available = stream.available();
			if(count > available) {
				bytesToRead = available;
			}
		}
		readBuffer = new byte[bytesToRead];
		stream.read(readBuffer);
		return new String(readBuffer);
	}
	private String readFromInputStreamOrExit(InputStream stream, int count, boolean block)
	{
		try {
			return readFromInputStream(stream, count, block);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private String readLineFromReader(BufferedReader reader) throws IOException
	{
		return reader.readLine();
	}
	private String readLineFromReaderOrExit(BufferedReader reader)
	{
		try {
			return readLineFromReader(reader);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	public String readAllFromStdOut() throws IOException
	{
		return readFromInputStream(xStdOut, xStdOut.available(), false);
	}
	public String readAllFromStdOutOrExit()
	{
		try {
			return readFromInputStreamOrExit(xStdOut, xStdOut.available(), false);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	public String readFromStdOut(int count, boolean block) throws IOException
	{
		return readFromInputStream(xStdOut, count, block);
	}
	public String readFromStdOutOrExit(int count, boolean block) 
	{
		return readFromInputStreamOrExit(xStdOut, count, block);
	}
	public String readLineFromStdOut() throws IOException
	{
		return readLineFromReader(xStdOutReader);
	}
	public String readLineFromStdOutOrExit()
	{
		return readLineFromReaderOrExit(xStdOutReader);
	}
	public String readAllFromStdErr() throws IOException
	{
		return readFromInputStream(xStdErr, xStdErr.available(), false);
	}
	public String readAllFromStdErrOrExit()
	{
		try {
			return readFromInputStreamOrExit(xStdErr, xStdErr.available(), false);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	public String readFromStdErr(int count, boolean block) throws IOException
	{
		return readFromInputStream(xStdErr, count, block);
	}
	public String readFromStdErrOrExit(int count, boolean block) 
	{
		return readFromInputStreamOrExit(xStdErr, count, block);
	}
	public String readLineFromStdErr() throws IOException
	{
		return readLineFromReader(xStdErrReader);
	}
	public String readLineFromStdErrOrExit()
	{
		return readLineFromReaderOrExit(xStdErrReader);
	}
}
